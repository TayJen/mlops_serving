FROM python:3.9

WORKDIR /code

RUN pip install --upgrade pip
# RUN pip install poetry

# COPY ./pyproject.toml /code/
# COPY ./poetry.lock /code/
COPY ./requirements.txt /code/

COPY ./inference.py /code/app/inference.py
# COPY ./.env /code/app/.env

# RUN poetry install --no-interaction --no-ansi
RUN pip install -r requirements.txt
CMD ["uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]
# CMD ["poetry", "run", "uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]
