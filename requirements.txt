click==8.1.7
Sphinx==7.3.7
coverage==7.5.1
flake8==7.0.0
mypy==1.10.0
python-dotenv==1.0.1

black==24.4.2
ruff==0.4.4

catboost==1.0.6
xgboost==1.5.2
matplotlib==3.5.1
numpy==1.22.4
pandas==1.4.2
scikit_learn==1.1.1
seaborn==0.11.2
mlflow==2.13.1
dvc-s3==3.2.0
fastapi==0.96.1
uvicorn==0.28.1
prometheus-client==0.17.0
prometheus-fastapi-instrumentator==6.0.0
python-multipart==0.0.5

